from gan_constructor.classes.NiftiFromZipExtractor import NiftiFromZipExtractor
from gan_constructor.classes.PngFromZipExtractor import PngFromZipExtractor


class MriGAN(NiftiFromZipExtractor, PngFromZipExtractor):
    def __init__(self):
        NiftiFromZipExtractor.__init__(self)
        PngFromZipExtractor.__init__(self)

    def run(self):
        # MriGAN.extract_nifti_from_zip(self)
        MriGAN.extract_png_from_zip(self)


if __name__ == '__main__':
    MriGAN().run()
