import os

zip_data_directory = os.path.join(os.getcwd(), 'data', 'zips', 'input')
intermediate_zip_data_directory = os.path.join(os.getcwd(), 'data', 'zips', 'intermediate')
unzipped_data_directory = os.path.join(os.getcwd(), 'data', 'zips', 'output')

png_data_directory = os.path.join(os.getcwd(), 'data', 'mri', 'output')