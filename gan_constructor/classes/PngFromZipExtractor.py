import os
import nibabel as nib
import matplotlib.pyplot as plt
import numpy as np

import gan_constructor.constants as constants


class PngFromZipExtractor:
    def __init__(self):
        print("INITIALIZING PngFromZipExtractor")
        self.input_directory = constants.unzipped_data_directory
        self.output_directory = constants.png_data_directory

    def extract_png_from_zip(self):
        for my_tesla_type in ['3T', '7T']:
            for my_t_type in ['t1', 't2']:
                my_nifti_directory = os.path.join(self.input_directory, my_tesla_type, my_t_type)
                for my_file in os.listdir(my_nifti_directory):
                    my_file_path = os.path.join(my_nifti_directory, my_file)
                    my_img = nib.load(my_file_path)
                    my_img_data = my_img.get_fdata()
                    my_png_directory = os.path.join(self.output_directory, my_tesla_type, my_t_type)
                    self.get_png_slices_from_img_data(my_img_data, my_file, my_png_directory)
                    # os.remove(my_file)

    @staticmethod
    def get_png_slices_from_img_data(a_img_data, a_file, a_png_directory):
        my_id = a_file.split('_')[0]
        for i in range(a_img_data.shape[0]):
            if (i % 5 == 0) and (i/5 >= 10) and (i/5 < 40):
                my_png_name = my_id+'_x_'+str(int(i / 5)-9)+'.png'
                my_png_path = os.path.join(a_png_directory, my_png_name)
                plt.imshow(a_img_data[i, 25:25+256, :256].T, cmap="gray", origin="lower")
                plt.axis('off')
                plt.savefig(my_png_path, bbox_inches='tight', pad_inches=0)
                plt.close()

        for i in range(a_img_data.shape[1]):
            if (i % 5 == 0) and (i/5 >= 15) and (i/5 < 45):
                my_png_name = my_id+'_y_'+str(int(i / 5)-14)+'.png'
                my_png_path = os.path.join(a_png_directory, my_png_name)
                plt.imshow(a_img_data[:256, i, :256].T, cmap="gray", origin="lower")
                plt.axis('off')
                plt.savefig(my_png_path, bbox_inches='tight', pad_inches=0)
                plt.close()

        for i in range(a_img_data.shape[2]):
            if (i % 5 == 0) and (i/5 >= 15) and (i/5 < 40):
                my_png_name = my_id + '_z_' + str(int(i / 5)-14) + '.png'
                my_png_path = os.path.join(a_png_directory, my_png_name)
                plt.imshow(a_img_data[:256, 25:25+256, i].T, cmap="gray", origin="lower")
                plt.axis('off')
                plt.savefig(my_png_path, bbox_inches='tight', pad_inches=0)
                plt.close()


if __name__ == '__main__':
    PngFromZipExtractor().extract_png_from_zip()
