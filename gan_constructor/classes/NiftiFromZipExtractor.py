import os
import zipfile
import shutil
import gan_constructor.constants as constants


class NiftiFromZipExtractor:
    def __init__(self):
        print("INITIALIZING NiftiFromZipExtractor")
        self.input_directory_zip = constants.zip_data_directory
        self.intermediate_directory_zip = constants.intermediate_zip_data_directory
        self.output_directory_zip = constants.unzipped_data_directory

    def extract_nifti_from_zip(self):
        my_file_list = os.listdir(self.input_directory_zip)
        my_file_list.sort()

        for my_file in my_file_list:
            if '.zip' in my_file:
                my_unzip_result = self.unzip_file_to_intermediate_directory_zip(my_file)
                if my_unzip_result != 'error':
                    self.move_required_files_from_intermediate_to_output_directory_t3(my_file)
                    self.move_required_files_from_intermediate_to_output_directory_t7(my_file)
                    self.empty_intermediate_directory(my_file)

    def unzip_file_to_intermediate_directory_zip(self, a_file):
        my_zipped_directory = os.path.join(self.input_directory_zip, a_file)
        try:
            with zipfile.ZipFile(my_zipped_directory, 'r') as my_zip_ref:
                my_zip_ref.extractall(self.intermediate_directory_zip)
            return None
        except Exception as e:
            print(a_file, e)
            return 'error'

    def move_required_files_from_intermediate_to_output_directory_t3(self, a_file):
        my_patient_id = a_file.split('_')[0]
        my_unzip_main_folder = os.path.join(self.intermediate_directory_zip, my_patient_id, 'T1w')
        for my_file in os.listdir(my_unzip_main_folder):
            if my_file == 'T1w_acpc_dc.nii.gz':
                my_old_directory = os.path.join(my_unzip_main_folder, my_file)
                my_new_directory = os.path.join(self.output_directory_zip, '3T', 't1', '_'.join([my_patient_id, my_file]))
                shutil.move(my_old_directory, my_new_directory)
            if my_file == 'T2w_acpc_dc.nii.gz':
                my_old_directory = os.path.join(my_unzip_main_folder, my_file)
                my_new_directory = os.path.join(self.output_directory_zip, '3T', 't2', '_'.join([my_patient_id, my_file]))
                shutil.move(my_old_directory, my_new_directory)

    def move_required_files_from_intermediate_to_output_directory_t7(self, a_file):
        my_patient_id = a_file.split('_')[0]
        my_unzip_main_folder = os.path.join(self.intermediate_directory_zip, my_patient_id, 'T1w')
        for my_file in os.listdir(my_unzip_main_folder):
            if my_file == 'T1w_acpc_dc.nii.gz':
                my_old_directory = os.path.join(my_unzip_main_folder, my_file)
                my_new_directory = os.path.join(self.output_directory_zip, '3T', 't1', '_'.join([my_patient_id, my_file]))
                shutil.move(my_old_directory, my_new_directory)
            if my_file == 'T2w_acpc_dc.nii.gz':
                my_old_directory = os.path.join(my_unzip_main_folder, my_file)
                my_new_directory = os.path.join(self.output_directory_zip, '3T', 't2', '_'.join([my_patient_id, my_file]))
                shutil.move(my_old_directory, my_new_directory)

    def empty_intermediate_directory(self, a_file):
        my_patient_id = a_file.split('_')[0]
        my_useless_data = os.path.join(self.intermediate_directory_zip, my_patient_id)
        shutil.rmtree(my_useless_data)


if __name__ == '__main__':
    NiftiFromZipExtractor().extract_nifti_from_zip()
